#include <stdio.h>

void CountAndPrintCharsSpasesLines(FILE * file);
int main(void)
{
	char fileName[] = "cla_lab_7_3_8_2-B.c";
	FILE *file;

	file=fopen(fileName, "r");
	if (file == NULL)
	{
		printf("\nError, Unable to open the file for reading\n");
		return 1;
	}

	CountAndPrintCharsSpasesLines(file);

	fclose(file);
	return 0;
}

void CountAndPrintCharsSpasesLines(FILE * file)
{
	char charWritedFromFile = 0;
	int whiteSpases = 0;
	int words = 0;
	int charsCount[26]={0};
	int linesCount = 1;
	
	while((charWritedFromFile = fgetc(file)) != EOF)
	{

		if (charWritedFromFile == '\n')
		{
			linesCount++;
			continue;
		}

		if (charWritedFromFile== ' ')
		{
			whiteSpases++;
			continue;
		 }
		
		words++;

		if(charWritedFromFile >= 'a' && charWritedFromFile <= 'z')
		{
			charsCount[charWritedFromFile - 'a']++;
		}
	}
	
	printf("Lines: %d\n", linesCount);
	printf("Whitespaces: %d \n", whiteSpases);
	printf("Characters :%d\n", words);

	for(int i =0; i<26;i++)
	{
		printf("Small letter : %c : %d\n", (char)('a'+i), charsCount[i]);
	}
}