#include <stdio.h>
#include <stdlib.h>


void CopyFile1IntoFile2(FILE *file1, FILE *file2);
void FillArrayFromTo(char from[], char *toArr, int start, int finish);
void GetNameAndExtensionSepareted(char from[], char **name, char **extention);

int main(){
	FILE *f1, *f2;
	char fileNameToGetCopy[] = "cla_lab_7_3_8_3-B.c", *fileNameCopy, *fileNameCopyExtension;
	char fileNameCopyFull[] = "cla_lab_7_3_8_3-B.c";

	GetNameAndExtensionSepareted(fileNameCopyFull, &fileNameCopy, &fileNameCopyExtension);

	f1 = fopen(fileNameToGetCopy, "r");
	if (f1 == NULL){
		printf("Error opening a file.");
		return 1;
	}

	f2 = fopen(fileNameCopyFull, "r");
	if (f2 != NULL){
		int count=1;
		do{
			fclose(f2);
			snprintf(fileNameCopyFull, 256, "%s%d%c%s", fileNameCopy, count++, '.', fileNameCopyExtension);
			f2 = fopen(fileNameCopyFull, "r");
			if(count > 512)
			{
				printf("File count limit reached.");
				return 2;
			}
		}while(f2 != NULL);
	}

	f2 = fopen(fileNameCopyFull, "w");
	if (f2 == NULL){
		fclose(f1);
		printf("Error opening a file.");
		return 1;
	}

	CopyFile1IntoFile2(f1, f2);

	fclose(f1);
	fclose(f2);
	
	return 0;
}
void CopyFile1IntoFile2(FILE *file1, FILE *file2)
{
	char line[256];
    while (fgets(line, sizeof(line), file1)!=NULL) 
	{
		fprintf(file2, "%s", &line);
    }
}
void GetNameAndExtensionSepareted(char from[], char **name, char **extention)
{
	int indexLastSymbol = 255;
	int pointIndex = 0;

	for(int i = 0; i<256; i++)
	{
		if(from[i]=='\0')
		{
			indexLastSymbol = i;
			break;
		}
		if(from[i]=='.')
		{
			pointIndex = i;
		}
	}

	int nameLen = pointIndex + 1;
	int extentionLen = indexLastSymbol - pointIndex;

	*name = calloc(nameLen, sizeof(char));
	*extention = calloc(extentionLen, sizeof(char));

	FillArrayFromTo(from, *name, 0, nameLen-1);
	FillArrayFromTo(from, *extention, pointIndex + 1, indexLastSymbol);
}
void FillArrayFromTo(char from[], char *toArr, int start, int finish)
{
	for(int i=start; i<finish; i++)
	{
		toArr[i - start]=from[i];
	}
	toArr[finish - start] = '\0';
}