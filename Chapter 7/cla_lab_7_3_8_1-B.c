#include <stdio.h>

int main()
{
	FILE *f1, *f2;
	char *fileNameToGetCopy, *fileNameCopy;
	char line[256];
	
	fileNameToGetCopy = "cla_lab_7_3_8_1-B.c";
	fileNameCopy = "cla_lab_7_3_8_1-B_copy.txt";

	f1 = fopen(fileNameToGetCopy, "r");
	if (f1 == NULL){
		printf("Error opening a file.");
		return 1;
	}

	f2 = fopen(fileNameCopy, "w");
	if (f2 == NULL){
		fclose(f1);
		printf("Error opening a file.");
		return 1;
	}

	int i = 1;
	while (fgets(line, sizeof(line), f1)!=NULL)
	{
		fprintf(f2, "%d %s", i, &line);
		i++;
	}

	fclose(f1);
	fclose(f2);
	
	return 0;
}
