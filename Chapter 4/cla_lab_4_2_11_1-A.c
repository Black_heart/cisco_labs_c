#include <stdio.h>

int main()
{
	int fibSeq[11], i;
	fibSeq[0] = 1;
	fibSeq[1] = 1;

	for (i = 2; i < 11; ++i)
		fibSeq[i] = fibSeq[i - 1] + fibSeq[i - 2];
	for (i = 0; i < 10; ++i)
		printf("%d\n", fibSeq[i]);
	for (i = 0; i < 10; i += 2)
		printf("%d\n", fibSeq[i]);
	for (i = 1; i < 10; i += 2)
		printf("%d\n", fibSeq[i]);

	return 0;
}
