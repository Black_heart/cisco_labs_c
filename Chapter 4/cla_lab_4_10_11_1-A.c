#include <stdio.h>
#include <string.h>

int main()
{
	char row[40], tmp;
	scanf("%s", row);
	int length = strlen(row);

	for (int i = 0; i < length / 2; ++i)
	{
		tmp = row[i];
		row[i] = row[length - 1 - i];
		row[length - 1 - i] = tmp;
	}
	printf("%s\n", row);
	return 0;
}