#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int validateIpAddr(char *ipAddr);
void print_addr(char*);
int find_dot(char* addr, int start_index);

int main()
{
	char IPaddr[20];
	scanf("%s", IPaddr);
	if (validateIpAddr(IPaddr))
		print_addr(IPaddr);
	else
		printf("Error: not a valid address.");

	return 0;
}

int validateIpAddr(char *ipAddr)
{
	int ipSection[4] = { 0, 0, 0, 0 };
	char c = '\0';
	if (sscanf(ipAddr, "%d.%d.%d.%d%s", &ipSection[0], &ipSection[1], &ipSection[2], &ipSection[3], &c) > 3 && c == '\0')
	{
		for (int i = 0; i < 4; i++)
		{
			if (ipSection[i] > 255 || ipSection[i] < 0)
			{
				return 0;
			}
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

void print_addr(char* ip_addr)
{
	int point_index = 0;
	for (size_t i = 3; i > 0; --i)
	{
		point_index = find_dot(ip_addr, point_index);		
		printf("Last %d parts: %s\n", i, ip_addr + point_index);
	}
	puts("");
}

int find_dot(char* addr, int start_index)
{
	int i = start_index;
	while (addr[++i] != '.');
	return i;
}