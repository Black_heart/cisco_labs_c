#include <stdio.h>

int main()
{
	int num1, num2, num3, num4, checker;
	char IPaddr[16];
	scanf("%d\n%d\n%d\n%d", &num1, &num2, &num3, &num4);
	
	if ((num1 >= 0 && num1 <= 255) &&
		(num2 >= 0 && num2 <= 255) &&
		(num3 >= 0 && num3 <= 255) &&
		(num4 >= 0 && num4 <= 255))
	{
		sprintf(IPaddr, "%d.%d.%d.%d", num1, num2, num3, num4);
		printf("%s", IPaddr);
	}
	else
		printf("Error! Please, enter numbers again.");
	return 0;
}