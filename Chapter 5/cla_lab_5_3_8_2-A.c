#include <stdio.h>

int main()
{
	int number;
	scanf("%d", &number);
	if (number > 6 || number < 0)
	{
		printf("Error, no such day.");
		return 0;
	}

	char *days[] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
	printf("Pointer version: %s\n", *(days + number));
	printf("Array index version: %s", days[number]);

	return 0;
}