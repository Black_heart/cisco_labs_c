#include <stdio.h>
#include <stdlib.h>

int main()
{
	int number, memory;
	scanf("%d", &number);
	if (number >= 1024 * 1024)
	{
		printf("Too much memory requested.");
		return 0;
	}

	char* charactArr = (char*)malloc(number);
	for (int i = 0; i < number; ++i)
		charactArr[i] = 'A' + (i % 26);

	if (number <= 400)
		memory = number;
	else
		memory = 400;
	for (int i = 0; i < memory; i += 40)
	{
		if (memory - i <= 40)
			printf("%.*s\n", memory - i, charactArr + i);
		else
			printf("%.*s\n", 40, charactArr + i);
	}
	free(charactArr);
	return 0;
}