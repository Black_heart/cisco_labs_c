#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int validateIpAddr(char *ipAddr);

int main()
{
	char ip1[] = "127.0.0.1";
	char ip2[] = "127.0.01";
	char ip3[] = "127.0..1";
	char ip4[] = "127.297.100.abc";
	char ip5[] = "127.247.100.234";
	printf("%s", ip1);
	validateIpAddr(ip1) ? printf(" is a valid IP address\n"): printf(" is not a valid IP address\n");
	printf("%s", ip2);
	validateIpAddr(ip2)? printf(" is a valid IP address\n"): printf(" is not a valid IP address\n");
	printf("%s", ip3);
	validateIpAddr(ip3)? printf(" is a valid IP address\n"): printf(" is not a valid IP address\n");
	printf("%s", ip5);
	validateIpAddr(ip5)? printf(" is a valid IP address\n"): printf(" is not a valid IP address\n");
	return 0;
}

int validateIpAddr(char *ipAddr)
{
	int ipSection[4] = {0, 0, 0, 0};
	char c = '\0';
	if(sscanf(ipAddr, "%d.%d.%d.%d%s", &ipSection[0], &ipSection[1], &ipSection[2], &ipSection[3], &c) > 3 && c == '\0')
	{
		for(int i = 0; i < 4; i++)
		{
			if(ipSection[i] > 255 || ipSection[i] < 0)
			{
				return 0;
			}
		}
		return 1;
	}
	else
	{
		return 0;
	}
}
