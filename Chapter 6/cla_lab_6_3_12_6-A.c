#include <stdio.h>
int stringCompare(char *s1, char *s2)
{
    int num;
    for (num = 0; s1[num]!=0 && s2[num]!=0; num++)
    {
        if(s1[num]>s2[num])
        {
        return 1;
        }
        else if (s1[num]<s2[num])
        {
        return -1;
        }
    }

    if (s1[num] == 0)
    {
        if (s2[num] == 0)
        return 0;
        else  return -1;
    }
    else  return 1;
}

int main(void)
{
    int result1 = stringCompare("AAA", "BBB");
    int result2 = stringCompare("AAC", "AAB");
    int result3 = stringCompare("AAC", "AAC");
    int result4 = stringCompare("AAC", "AACC");
    printf("result1: %d\n", result1);
    printf("result2: %d\n", result2);
    printf("result3: %d\n", result3);
    printf("result4: %d\n", result4);
    return 0;
}
