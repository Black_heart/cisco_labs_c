#include <stdio.h>
double sqrt(double n);

int main()
{	
	printf("square of %.2f is %.2f\n", 2., sqrt(2.));
	printf("square of %.2f is %.2f\n", 6., sqrt(6.));
	printf("square of %.2f is %.2f\n", 2.50, sqrt(2.50));
	printf("square of %.2f is %.2f\n", 12.12, sqrt(12.12));
	printf("square of %.2f is %.2f\n", 345.68, sqrt(345.68));
	printf("square of %.2f is %.2f\n", 6969.150, sqrt(6969.150));
	return 0;
}

double sqrt(double n)
{
	return n * n;
}






