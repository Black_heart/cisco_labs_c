#include <stdio.h>
int main()
{
int daysInCrntFeb = 29;
int daysInJan = 31;
int daysInFeb = daysInCrntFeb;
int daysInMar = 31;
int daysInApr = 30;
int daysInMay = 31;
int daysInJun = 30;
int daysInJul = 31;
int daysInAug = 31;
int daysInSep = 30;
int daysInOct = 31;
int daysInNov = 30;
int daysInDec = 31;
int daysInFirstHalf = daysInJan + daysInFeb + daysInMar
+ daysInApr + daysInMay + daysInJun;
int daysInSecondHalf = daysInJul + daysInAug + daysInSep
+ daysInOct + daysInNov + daysInDec;
printf("Days in the first half of the current year: %d\n", daysInFirstHalf);
printf("Days in the second half of the current year: %d\n", daysInSecondHalf);
printf("Days in the current year: %d\n", daysInFirstHalf + daysInSecondHalf);
return 0;
}
