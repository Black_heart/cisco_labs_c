#include<stdio.h>

int main()
{
	int crnt_feb = 28;
	int jan= 31;
	int feb = 29;
	int apr = 30;
	int mar = 31;
	int may = 31;
	int jul = 31;
	int aug = 31;
	int oct = 31;
	int dec = 31;
	int jun = 30;
	int sep = 30;
	int nov = 30;
	int quarter_1_current = jan + crnt_feb + mar;
	int quarter_1 = jan + feb + mar;
	int quarter_2 = apr + may + jun;
	int quarter_3 = jul + aug + sep;
	int quarter_4 = oct + nov + dec;
	
	printf("Days in quarters (2019)\n");
	printf("Days in Q1 of the leap year/current year: %d / %d\n", quarter_1, quarter_1_current);
	printf("Days in Q2 of the current year: %d\n", quarter_2);
	printf("Days in Q3 of the current year: %d\n", quarter_3);
	printf("Days in Q4 of the current year: %d\n\n", quarter_4);
	
	return 0;
}
