#include <stdio.h>
int main()
{
float startVal = 100;
float interestRate = 0.015;
float firstYearVal;
float secondYearVal;
float thirdYearVal;
firstYearVal = startVal + startVal * interestRate;
secondYearVal = firstYearVal + firstYearVal * interestRate;
thirdYearVal = secondYearVal + secondYearVal * interestRate;
printf("After first year: %f\n", firstYearVal);
printf("After second year: %f\n", secondYearVal);
printf("After third year: %f\n", thirdYearVal);
return 0;
}
