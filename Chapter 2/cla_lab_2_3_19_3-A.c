#include <stdio.h>
#include <stdlib.h>

int main()
{
    char zero = '0';

    for(int i = 0; i <= 9; i++)
    {
        printf("\'%d\' - \'%c\' is: %d\n", i,zero,i);
    }
    
    return 0;
}
