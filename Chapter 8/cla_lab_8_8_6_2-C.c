#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IP_SCHEME "%d.%d.%d.%d%s"
#define IP1 "127.0.0.1"
#define IP2 "127.0.01"
#define IP3 "127.0..1"
#define IP4 "127.297.100.abc"
#define IP5 "127.247.100.234"
#define VALIDE " is a valid IP address\n"
#define INVALIDE " is not a valid IP address\n"
#define PRINT_STRING(s) printf("%s", s);
#define PRINT_IS_IP_VALID(ip) printf(validateIpAddr(ip) ? VALIDE : INVALIDE);
#define WRITE_IP_AND_VALIDATION(ip) PRINT_STRING(ip) PRINT_IS_IP_VALID(ip)
#define IS_INT_NOT_IN_IP_RANGE(i) (i) > 255 || (i) < 0
#define ZERO_SYMBOL '\0'

int validateIpAddr(char* ipAddr);

int main()
{
	WRITE_IP_AND_VALIDATION(IP1)
	WRITE_IP_AND_VALIDATION(IP2)
	WRITE_IP_AND_VALIDATION(IP3)
	WRITE_IP_AND_VALIDATION(IP4)
	WRITE_IP_AND_VALIDATION(IP5)
	return 0;
}

int validateIpAddr(char* ipAddr)
{
	int ipSection[4];
	char c = ZERO_SYMBOL;
	if (sscanf(ipAddr, IP_SCHEME, &ipSection[0], &ipSection[1], &ipSection[2], &ipSection[3], &c) > 3 && c == ZERO_SYMBOL)
	{
		for (int i = 0; i < 4; i++)
		{
			if (IS_INT_NOT_IN_IP_RANGE(ipSection[i]))
			{
				return 0;
			}
		}
		return 1;
	}
	else
	{
		return 0;
	}
}