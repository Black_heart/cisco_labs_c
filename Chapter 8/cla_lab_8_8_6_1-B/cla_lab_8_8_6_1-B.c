#include <stdio.h>
#include "cla_lab_8_8_6_1-B_simple_triangle.h"
#include "cla_lab_8_8_6_1-B_floyds_triangle.h"

int main(int argc, char *argv[])
{
	int n = 0;
	scanf("%d", &n);
	printf(GET_FUNC_LOG("print_simple_triangle"));
	print_simple_triangle(n);
	printf(GET_FUNC_LOG("print_floyds_triange"));
	print_floyds_triange(n);
	return 0;
}

