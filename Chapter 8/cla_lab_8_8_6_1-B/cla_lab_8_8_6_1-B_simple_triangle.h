#ifndef __SIMPLE_TRIANGLE_H__
#define __SIMPLE_TRIANGLE_H__
#define GET_FUNC_LOG(func) "In line %d, file "__FILE__", before the normalTriangle "func"\n",__LINE__

void print_simple_triangle(int rows);
#endif